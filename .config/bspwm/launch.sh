#!/usr/bin/env bash
# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar
polybar internal &

my_laptop_external_monitor=$(xrandr --query | grep 'HDMI-1-0')
if [[ $my_laptop_external_monitor = *connected* ]]; then
    polybar external &
fi

nitrogen --restore &
google-chrome-stable &
emacs &
alacritty &
