 local base = require("plugins.configs.lspconfig")
 local on_attach = require("plugins.configs.lspconfig").on_attach
 local capabilities = require("plugins.configs.lspconfig").capabilities
 local lspconfig = require("lspconfig")
 local util = require "lspconfig/util"
 local config = require("plugins.configs.lspconfig")

 local on_attach_p = config.on_attach
 local capabilities_p = config.capabilities

 local on_attach_js = config.on_attach
 local capabilities_js = config.capabilities

 lspconfig.clangd.setup{
   on_attach = function (client, bufnr)
     client.server_capabilities.signatureHelpProvider = false
     on_attach(client, bufnr)
   end,
   capabilities = capabilities,
   
 }

 lspconfig.pyright.setup({
   on_attach = on_attach_p,
   capabilities = capabilities_p,
   filetypes = {"python"},
 })

 lspconfig.tsserver.setup {
   on_attach = on_attach_js,
   capabilities = capabilities_js,
   init_options = {
     preferences = {
       disableSuggestions = true,
     }
   }
 }
